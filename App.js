import { StyleSheet, Text, View } from "react-native";
import { Provider } from "react-redux";
import store from "./store";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Login from "./screens/Login";
import Home from "./screens/Home";
import Register from "./screens/Register";
import MenuDetail from "./screens/MenuDetail";
import AddMenu from "./screens/AddMenu";
import AddCategory from "./screens/AddCategory";
import CategoryDetail from "./screens/CategoryDetail";
import QRCode from "./screens/QRCode";
import Sukses from "./screens/Sukses";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="Register"
            component={Register}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="Home"
            component={Home}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="Menu Detail"
            component={MenuDetail}
            options={{
              headerStyle: {
                backgroundColor: "#b22222",
              },
              headerTintColor: "white",
            }}
          />

          <Stack.Screen
            name="Add Menu"
            component={AddMenu}
            options={{
              headerStyle: {
                backgroundColor: "#b22222",
              },
              headerTintColor: "white",
            }}
          />

          <Stack.Screen
            name="Add Category"
            component={AddCategory}
            options={{
              headerStyle: {
                backgroundColor: "#b22222",
              },
              headerTintColor: "white",
            }}
          />

          <Stack.Screen
            name="Category Detail"
            component={CategoryDetail}
            options={{
              headerStyle: {
                backgroundColor: "#b22222",
              },
              headerTintColor: "white",
            }}
          />

          <Stack.Screen
            name="QRCode"
            component={QRCode}
            options={{
              headerStyle: {
                backgroundColor: "#b22222",
              },
              headerTintColor: "white",
            }}
          />

          <Stack.Screen
            name="Sukses"
            component={Sukses}
            options={{
              headerStyle: {
                backgroundColor: "#b22222",
              },
              headerTintColor: "white",
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "center",
    justifyContent: "center",
  },
});
