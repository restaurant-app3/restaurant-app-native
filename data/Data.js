export const dummyData = [
  {
    title: "BBQ Chicken",
    url: "https://img.freepik.com/free-photo/chicken-wings-barbecue-sweetly-sour-sauce-picnic-summer-menu-tasty-food-top-view-flat-lay_2829-6471.jpg?w=2000",
    description:
      "Roasted chicken is chicken meat that is cooked as a dish by roasting. Either baked in the home kitchen, over the fire, or on the rotisserie.",
    id: 1,
  },
  {
    title: "Central Texas Barbecue",
    url: "https://www.washingtonian.com/wp-content/uploads/2021/07/2Fiftys-1500x1000.jpg",
    description:
      "Riverdale Park’s 2Fifty Texas BBQ has been luring ’cue fans with its glorious brisket since opening last year.",
    id: 2,
  },
  {
    title: "French foods",
    url: "https://www.expatica.com/app/uploads/sites/5/2014/05/french-food.jpg",
    description:
      "Dishes don’t get much more typically French than bœuf bourguignon. The dish hails from the same region as coq au vin.",
    id: 3,
  },
  {
    title: "Appetizers",
    url: "https://images.unsplash.com/photo-1600891964599-f61ba0e24092?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YXBwZXRpemVyc3xlbnwwfHwwfHw%3D&w=1000&q=80",
    description:
      "A whole bunch of food that got cold. Shots of tequila, salads, burgers with satanic pineaples, lots of spilled salt and some more tequila.",
    id: 4,
  },
];
