import { View, Text, StyleSheet } from "react-native";
import React from "react";
import { convertCurrency } from "../util/convert";

export default function CartTotal({ item }) {
  return (
    <View style={styles.totals}>
      <Text style={styles.name}>{item.name} </Text>
      <Text style={styles.subtotal}>
        {convertCurrency(
          item.price * item.quantity -
            item.price * item.quantity * (item.discount / 100)
        )}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  totals: {
    flexDirection: "row",
    paddingHorizontal: 20,
  },

  name: {
    flex: 1,
  },

  subtotal: {
    alignItems: "flex-end",
  },
});
