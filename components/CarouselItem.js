import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";

const CarouselItem = ({ item }) => {
  return (
    <View style={styles.cardView}>
      <Image style={styles.image} source={{ uri: item.url }} />
      <View style={styles.textView}>
        <Text style={styles.itemTitle}> {item.title}</Text>
        <Text style={styles.itemDescription}>{item.description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    backgroundColor: "white",
    margin: 10,
    borderRadius: 10,
    elevation: 5,
  },

  textView: {
    position: "absolute",
    bottom: 10,
    margin: 10,
    left: 5,
  },

  image: {
    width: 405,
    height: 250,
    borderRadius: 10,
  },

  itemTitle: {
    color: "white",
    fontSize: 22,
    marginBottom: 5,
    fontWeight: "bold",
    elevation: 5,
  },

  itemDescription: {
    color: "white",
    fontSize: 12,
    elevation: 5,
  },
});

export default CarouselItem;
