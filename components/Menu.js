import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import { convertCurrency } from "../util/convert";
import { FontAwesome } from "@expo/vector-icons";

export default function Menu({ item, navigation }) {
  const goToMenuDetail = () => {
    navigation.navigate("Menu Detail", item);
  };

  return (
    <View style={styles.all}>
      <TouchableOpacity onPress={goToMenuDetail}>
        <View style={styles.card}>
          <View>
            <Image
              style={{
                width: 120,
                height: 100,
                borderRadius: 8,
              }}
              source={{
                uri: `${item.image}`,
              }}
            />
          </View>
          <View style={styles.content}>
            <Text style={styles.discount}>{item.discount}%</Text>
            <Text style={styles.title}>{item.name}</Text>
            <Text style={styles.price}>{convertCurrency(item.price)}</Text>
            <Text>
              <FontAwesome name="star" size={17} color="orange" /> {""} 4.8
            </Text>
          </View>
          <View style={styles.icon}>
            <Text style={{ position: "absolute", right: 8 }}>
              <AntDesign name="right" size={24} color="#b22222" />
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  all: {
    flex: 1,
  },

  card: {
    flex: 1,
    flexDirection: "row",
    padding: 15,
    borderRadius: 10,
    margin: 8,
    borderColor: "red",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },

  content: {
    flex: 7,
    marginLeft: 15,
  },

  discount: {
    width: 40,
    padding: 5,
    backgroundColor: "#b22222",
    color: "white",
    fontSize: 11,
    fontWeight: "bold",
    textAlign: "center",
    borderRadius: 5,
  },

  title: {
    marginTop: 5,
    fontSize: 17,
    fontWeight: "bold",
  },

  price: {
    lineHeight: 25,
  },

  icon: {
    flex: 1,
    justifyContent: "center",
  },
});
