import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";
import React from "react";
import { useDispatch } from "react-redux";
import { FontAwesome5 } from "@expo/vector-icons";
import { convertCurrency } from "../util/convert";

export default function Cart({ item }) {
  const dispatch = useDispatch();

  const deleteToCart = async () => {
    dispatch({ type: "DELETE_CART", payload: item.id });
  };

  return (
    <View>
      <View style={styles.card}>
        <View>
          <Image
            style={{
              width: 120,
              height: 100,
              borderRadius: 8,
            }}
            source={{
              uri: `${item.image}`,
            }}
          />
        </View>
        <View style={styles.content}>
          <Text style={styles.discount}>{item.discount}%</Text>
          <Text style={styles.title}>{item.name}</Text>
          <Text style={styles.price}>{convertCurrency(item.price)}</Text>

          <View style={styles.quantity}>
            <Text>Quantity : {item.quantity}</Text>
          </View>
        </View>

        <TouchableOpacity style={styles.icon} onPress={deleteToCart}>
          <Text style={{ position: "absolute", right: 10 }}>
            <FontAwesome5 name="trash" size={20} color="#b22222" />
          </Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.total}>
        Subtotal : {""}
        {convertCurrency(
          item.price * item.quantity -
            item.price * item.quantity * (item.discount / 100)
        )}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  all: {
    flex: 1,
  },

  card: {
    flex: 1,
    flexDirection: "row",
    padding: 15,
    borderRadius: 10,
    margin: 8,
    borderColor: "red",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },

  content: {
    flex: 7,
    marginLeft: 15,
  },

  discount: {
    width: 40,
    padding: 5,
    backgroundColor: "#b22222",
    color: "white",
    fontSize: 11,
    fontWeight: "bold",
    textAlign: "center",
    borderRadius: 5,
  },

  title: {
    marginTop: 5,
    fontSize: 17,
    fontWeight: "bold",
  },

  price: {
    lineHeight: 25,
  },

  quantity: {
    flexDirection: "row",
    alignSelf: "flex-end",
  },

  icon: {
    flex: 1,
  },

  total: {
    marginLeft: 23,
  },
});
