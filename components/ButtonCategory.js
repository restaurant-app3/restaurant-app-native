import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import React from "react";

export default function ButtonCategory({ item, navigation }) {
  const gotoCategoryDetail = () => {
    navigation.navigate("Category Detail", item);
  };

  return (
    <View>
      <TouchableOpacity onPress={gotoCategoryDetail}>
        <Text style={styles.name}>{item.name}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  name: {
    padding: 20,
    margin: 10,
    fontWeight: "bold",
    color: "#b22222",
    borderWidth: 1,
    borderColor: "#b22222",
    borderRadius: 10,
    fontSize: 16,
    textAlign: "center",
    marginHorizontal: 25,
  },
});
