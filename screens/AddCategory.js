import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from "react-native";
import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import { instance as axios } from "../util/api";
import { getStoreData } from "../util/Util";

export default function AddCategory({ navigation }) {
  const [name, setName] = useState("");

  const AddCategory = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        name: name,
      };

      await axios.post(`/api/categorys/add`, formData, {
        headers: {
          Authorization: `Bearer ${await getStoreData("token")}`,
        },
      });
      Alert.alert("Category successfully added");
    } catch (err) {
      console.log(err);
    }
    navigation.navigate("Home");
  };

  const save = (e) => {
    e.preventDefault();
    AddCategory();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.add}>
        <Ionicons name="restaurant" size={24} color="black" /> {""} AddCategory
      </Text>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          onChangeText={setName}
          value={name}
          placeholder="Name."
        />
      </View>

      <TouchableOpacity style={styles.saveBtn} onPress={save}>
        <Text style={styles.saveText}>SAVE</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },

  add: {
    alignSelf: "center",
    padding: 20,
    fontSize: 20,
    fontWeight: "bold",
  },

  inputView: {
    backgroundColor: "#2222",
    borderRadius: 5,
    width: "90%",
    height: 50,
    marginBottom: 20,
    alignSelf: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },

  saveBtn: {
    width: "25%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    backgroundColor: "#B22222",
    alignSelf: "center",
  },

  saveText: {
    fontWeight: "bold",
    fontSize: 15,
    color: "white",
  },
});
