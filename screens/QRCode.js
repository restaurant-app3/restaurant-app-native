import React from "react";
import { Text, View, StyleSheet, TouchableOpacity, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { instance as axios } from "../util/api";
import { getStoreData } from "../util/Util";
import QRCode from "react-native-qrcode-svg";

export default function QRcode({ navigation }) {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart.carts);

  const ok = async () => {
    try {
      const data = cart.map((item) => ({
        menuId: item.id,
        totalMenus: item.quantity,
      }));

      await axios.post(
        `/api/menus/${await getStoreData("id")}/transactions`,
        data,
        {
          headers: {
            Authorization: `Bearer ${await getStoreData("token")}`,
          },
        }
      );
      dispatch({ type: "RESET_CART" });
      Alert.alert("Successfully checkout");
      navigation.navigate("Menus");
      console.log("Berhasil di Checkout");
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleStyle}>Scan QR Code to make Payment</Text>

      <QRCode
        value={JSON.stringify(cart)}
        size={300}
        color="black"
        backgroundColor="white"
      />

      <TouchableOpacity style={styles.buttonStyle} onPress={ok}>
        <Text style={styles.buttonTextStyle}>OK</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    padding: 10,
  },

  titleStyle: {
    fontSize: 20,
    textAlign: "center",
    marginBottom: 40,
    fontWeight: "bold",
  },

  buttonStyle: {
    backgroundColor: "#B22222",
    alignItems: "center",
    borderRadius: 8,
    marginTop: 50,
    padding: 3,
  },

  buttonTextStyle: {
    color: "white",
    fontWeight: "bold",
    paddingVertical: 10,
    fontSize: 16,
    width: 90,
    textAlign: "center",
  },
});
