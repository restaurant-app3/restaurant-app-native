import { View, Text, Image, StyleSheet } from "react-native";
import React from "react";

export default function Sukses() {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={require("../assets/suksesRM.png")} />
      <Text style={styles.available1}>Menu Successfully Ordered</Text>
      <Text style={styles.available2}>Thank you for Ordering</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 180,
  },

  image: {
    width: 250,
    height: 250,
  },

  available1: {
    marginTop: 12,
    fontSize: 17,
    fontWeight: "bold",
  },

  available2: {
    marginTop: 3,
    fontSize: 17,
  },
});
