import { View, Text, Image, StyleSheet } from "react-native";
import React from "react";

export default function EmptyMenu() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require("../assets/pageNotFoundRM.png")}
      />
      <Text style={styles.available1}>Oops! Menu not Available</Text>
      <Text style={styles.available2}>
        Menu is not available in this category
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 150,
  },

  image: {
    width: 250,
    height: 250,
  },

  available1: {
    marginTop: 12,
    fontSize: 17,
    fontWeight: "bold",
  },

  available2: {
    marginTop: 3,
    fontSize: 17,
  },
});
