import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from "react-native";
import React from "react";
import { useSelector } from "react-redux";
import Cart from "../components/Cart";
import { convertCurrency } from "../util/convert";
import EmptyCart from "./EmptyCart";
import CartTotal from "../components/CartTotal";

export default function Carts({ navigation }) {
  const cart = useSelector((state) => state.cart.carts);
  const totalCart = useSelector((state) => state.cart.totalCart);

  const totalBayar = cart.reduce((acc, { price, quantity, discount }) => {
    const total = price * quantity;
    return acc + total - total * (discount / 100);
  }, 0);

  const checkout = async () => {
    navigation.navigate("QRCode");
  };

  return (
    <View>
      <ScrollView>
        {cart[0] ? (
          <View>
            {/* <Text style={styles.cart}>Shopping Cart : {totalCart}</Text> */}
            {cart.map((item, i) => (
              <Cart key={i} item={item} />
            ))}
            <Text style={styles.cartTotal}>
              Cart Totals : {convertCurrency(totalBayar)}
            </Text>
            <Text style={styles.hr}></Text>
            {cart.map((item, i) => (
              <CartTotal key={i} item={item} />
            ))}
            <TouchableOpacity style={styles.cartBtn} onPress={checkout}>
              <Text style={styles.cartText}>Checkout</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <EmptyCart />
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  cart: {
    fontSize: 19,
    fontWeight: "bold",
    marginLeft: 20,
    marginTop: 15,
    marginBottom: 15,
  },

  cartTotal: {
    fontSize: 19,
    fontWeight: "bold",
    paddingHorizontal: 20,
    marginTop: 30,
  },

  hr: {
    margin: 20,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: "red",
    marginBottom: 0,
  },

  cartBtn: {
    width: "92%",
    height: 50,
    alignSelf: "center",
    marginTop: 25,
    marginBottom: 15,
    borderRadius: 10,
    backgroundColor: "#b22222",
  },

  cartText: {
    fontWeight: "bold",
    fontSize: 18,
    textAlign: "center",
    marginTop: 10,
    color: "white",
  },
});
