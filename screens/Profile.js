import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import React from "react";
import { removeDataAsyncStorage } from "../util/Util";

export default function Profile({ navigation }) {
  const logout = () => {
    navigation.navigate("Login");
    removeDataAsyncStorage("token");
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}></View>
      <Image
        style={styles.avatar}
        source={{
          uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRT_bZVtu-PddY_qhswZvb8mjbcupdRazjY_w&usqp=CAU",
        }}
      />
      <View>
        <View style={styles.bodyContent}>
          <Text style={styles.name}>user@gmail.com</Text>
          <Text style={styles.info}>role : user</Text>
          <Text style={styles.description}>
            Lorem ipsum dolor sit amet, saepe sapientem eu nam. Qui ne assum
            electram expetendis, omittam deseruisse consequuntur ius an,
          </Text>

          <TouchableOpacity style={styles.buttonContainer} onPress={logout}>
            <Text style={styles.textLogout}>Logout</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  header: {
    backgroundColor: "#00BFFF",
    height: 200,
  },

  avatar: {
    width: 150,
    height: 150,
    borderRadius: 75,
    borderWidth: 4,
    borderColor: "#B22222",
    alignSelf: "center",
    marginTop: 130,
  },

  bodyContent: {
    flex: 1,
    alignItems: "center",
    padding: 30,
  },

  name: {
    fontSize: 28,
    fontWeight: "600",
  },

  info: {
    fontSize: 16,
    color: "#B22222",
    marginTop: 10,
  },

  description: {
    fontSize: 16,
    marginTop: 10,
    textAlign: "center",
  },

  buttonContainer: {
    marginTop: 30,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 150,
    borderRadius: 30,
    backgroundColor: "#B22222",
  },

  textLogout: {
    color: "white",
  },
});
