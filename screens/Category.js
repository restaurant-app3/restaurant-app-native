import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
} from "react-native";
import React, { useEffect, useState } from "react";
import { AntDesign } from "@expo/vector-icons";
import { instance as axios } from "../util/api";
import { getStoreData } from "../util/Util";
import ButtonCategory from "../components/ButtonCategory";
import { useIsFocused } from "@react-navigation/native";

export default function Add({ navigation }) {
  const [category, setCategory] = useState([]);
  const isFocused = useIsFocused();

  const fetchCategory = async () => {
    try {
      const { data, status } = await axios.get(`/api/categorys/`, {
        headers: {
          Authorization: `Bearer ${await getStoreData("token")}`,
        },
      });
      if (status === 200) {
        setCategory(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchCategory();
  }, [isFocused]);

  const gotoAddCAtegory = () => {
    navigation.navigate("Add Category");
  };

  return (
    <View style={styles.container}>
      {/* {getStoreData("role") === "admin" ? (
        <View style={styles.add}>
          <TouchableOpacity onPress={gotoAddCAtegory}>
            <Text style={styles.category}>
              <AntDesign name="pluscircle" size={20} color="white" /> Category
            </Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View></View>
      )} */}

      <Text style={styles.title}>
        Our Delicious <Text style={{ color: "#b22222" }}>Foods</Text>{" "}
      </Text>
      <Text style={styles.desc}>
        Food is any substance consumed to provide nutritional support for an
        organism.
      </Text>

      <FlatList
        data={category}
        renderItem={({ item }) => (
          <ButtonCategory
            item={item}
            setCategory={setCategory}
            navigation={navigation}
          />
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  add: {
    alignSelf: "center",
    marginTop: 20,
  },

  category: {
    backgroundColor: "#b22222",
    color: "white",
    padding: 10,
    borderRadius: 10,
    fontSize: 15,
  },

  title: {
    fontSize: 27,
    marginTop: 15,
    textAlign: "center",
  },

  desc: {
    fontSize: 16,
    marginTop: 10,
    textAlign: "center",
    marginBottom: 15,
  },
});
