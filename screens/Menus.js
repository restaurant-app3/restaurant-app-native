import {
  View,
  Text,
  StyleSheet,
  FlatList,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import React, { useEffect, useState } from "react";
import Menu from "../components/Menu";
import { instance as axios } from "../util/api";
import { getStoreData } from "../util/Util";
import { AntDesign } from "@expo/vector-icons";
import { dummyData } from "../data/Data";
import Carousel from "../components/Carousel";

export default function Menus({ navigation }) {
  const [menus, setMenus] = useState([]);

  // cara menggunakan async await
  const fetchMenus = async () => {
    try {
      const { data, status } = await axios.get("/api/menus/", {
        headers: {
          Authorization: `Bearer ${await getStoreData("token")}`,
        },
      });
      if (status === 200) {
        setMenus(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchMenus();
  }, []);

  const gotoAddMenu = () => {
    navigation.navigate("Add Menu");
  };

  return (
    <View style={styles.container}>
      {/* <StatusBar /> */}
      <Carousel data={dummyData} />

      {/* {getStoreData("role") === "admin" ? (
        <View style={styles.add}>
          <TouchableOpacity onPress={gotoAddMenu}>
            <Text style={styles.menu}>
              <AntDesign name="pluscircle" size={20} color="white" />
              {""} Menu
            </Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View></View>
      )} */}

      <Text style={styles.title}>
        Our Delicious <Text style={{ color: "#b22222" }}>Foods</Text>{" "}
      </Text>
      <Text style={styles.desc}>
        Food is any substance consumed to provide nutritional support for an
        organism.
      </Text>
      <FlatList
        data={menus}
        renderItem={({ item }) => (
          <Menu item={item} setMenus={setMenus} navigation={navigation} />
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  add: {
    alignSelf: "center",
    marginTop: 20,
  },

  menu: {
    backgroundColor: "#b22222",
    color: "white",
    padding: 10,
    borderRadius: 10,
    marginHorizontal: 15,
    fontSize: 15,
  },

  title: {
    fontSize: 27,
    marginTop: 10,
    textAlign: "center",
  },

  desc: {
    fontSize: 16,
    marginTop: 10,
    textAlign: "center",
    marginBottom: 15,
  },
});
