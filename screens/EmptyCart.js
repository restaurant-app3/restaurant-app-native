import { View, Text, Image, StyleSheet } from "react-native";
import React from "react";

export default function EmptyCart() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require("../assets/emptyCartRM.png")}
      />
      <Text style={styles.available1}>Oops! Cart Not Found</Text>
      <Text style={styles.available2}>
        Please add the menu to the cart first
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 180,
  },

  image: {
    width: 250,
    height: 250,
  },

  available1: {
    marginTop: 12,
    fontSize: 17,
    fontWeight: "bold",
  },

  available2: {
    marginTop: 3,
    fontSize: 17,
  },
});
