import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { FontAwesome, FontAwesome5, MaterialIcons } from "@expo/vector-icons";
import Profile from "./Profile";
import Menus from "./Menus";
import Carts from "./Carts";
import Category from "./Category";
import SearchBar from "./SearchBar";

const Tab = createBottomTabNavigator();

export default function Home() {
  return (
    <Tab.Navigator screenOptions={{ headerTitleAlign: "center" }}>
      <Tab.Screen
        name="Menus"
        component={Menus}
        options={{
          tabBarIcon: ({ color, size }) => (
            <FontAwesome5 name="home" size={22} color="#b22222" />
          ),
          headerStyle: {
            backgroundColor: "#b22222",
          },
          headerTintColor: "white",
        }}
      />

      <Tab.Screen
        name="Category"
        component={Category}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="category" size={25} color="#b22222" />
          ),
          headerStyle: {
            backgroundColor: "#b22222",
          },
          headerTintColor: "white",
        }}
      />

      <Tab.Screen
        name="SearchBar"
        component={SearchBar}
        options={{
          tabBarIcon: ({ color, size }) => (
            <FontAwesome5 name="search" size={22} color="#b22222" />
          ),
          headerStyle: {
            backgroundColor: "#b22222",
          },
          headerTintColor: "white",
        }}
      />

      <Tab.Screen
        name="Carts"
        component={Carts}
        options={{
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="shopping-cart" size={25} color="#b22222" />
          ),
          headerStyle: {
            backgroundColor: "#b22222",
          },
          headerTintColor: "white",
        }}
      />

      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({ color, size }) => (
            <FontAwesome5 name="user-alt" size={22} color="#b22222" />
          ),
          headerStyle: {
            backgroundColor: "#b22222",
          },
          headerTintColor: "white",
        }}
      />
    </Tab.Navigator>
  );
}
