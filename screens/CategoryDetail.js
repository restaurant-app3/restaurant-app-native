import { View, FlatList } from "react-native";
import React, { useEffect, useState } from "react";
import { getStoreData } from "../util/Util";
import { instance as axios } from "../util/api";
import Menu from "../components/Menu";
import EmptyMenu from "./EmptyMenu";

export default function CategoryDetail({ route, navigation }) {
  const [categorys, setCategorys] = useState([]);
  const { id } = route.params;

  const fetchCategoryId = async () => {
    try {
      const { data, status } = await axios.get(`/api/categorys/${id}`, {
        headers: {
          Authorization: `Bearer ${await getStoreData("token")}`,
        },
      });
      if (status === 200) {
        setCategorys(data.menu);
        // console.log(data.menu);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchCategoryId();
  }, []);

  return (
    <View>
      {categorys.length == 0 ? (
        <EmptyMenu />
      ) : (
        <FlatList
          data={categorys}
          renderItem={({ item }) => (
            <Menu
              item={item}
              setCategorys={setCategorys}
              navigation={navigation}
            />
          )}
        />
      )}
    </View>
  );
}
