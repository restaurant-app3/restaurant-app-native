import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from "react-native";
import { storeData } from "../util/Util";
import { instance as axios } from "../util/api";

export default function Register({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");

  const register = async () => {
    try {
      const { data, status } = await axios.post(`/register`, {
        email: email,
        password: password,
        role: role,
      });

      if (status === 200) {
        storeData("token", data.token);
        navigation.navigate("Login");
      } else {
        throw new Error("Register bermasalah");
      }
    } catch (err) {
      console.log(err);
      Alert.alert("Error login");
    }
  };

  const loginPage = () => {
    navigation.navigate("Login");
  };

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={require("../assets/login.png")} />
      <Text style={styles.welcome}>Registration your Account.</Text>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          onChangeText={setEmail}
          value={email}
          placeholder="Email."
        />
      </View>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          onChangeText={setPassword}
          value={password}
          secureTextEntry={true}
          placeholder="Password."
        />
      </View>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          onChangeText={setRole}
          value={role}
          secureTextEntry={true}
          placeholder="Role."
        />
      </View>

      <TouchableOpacity style={styles.loginBtn} onPress={register}>
        <Text style={styles.loginText}>REGISTER</Text>
      </TouchableOpacity>

      <View style={styles.acount}>
        <Text style={styles.dont}>Already have an account ? </Text>
        <TouchableOpacity>
          <Text style={styles.registerText} onPress={loginPage}>
            Login
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    width: 250,
    height: 250,
  },

  welcome: {
    color: "#B22222",
    fontSize: 22,
    marginBottom: 40,
  },

  inputView: {
    backgroundColor: "#2222",
    borderRadius: 5,
    width: "80%",
    height: 50,
    marginBottom: 20,
    // alignItems: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },

  forgot_button: {
    height: 30,
    marginTop: 10,
  },

  loginBtn: {
    width: "80%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    borderRadius: 5,
    backgroundColor: "#B22222",
  },

  loginText: {
    fontWeight: "bold",
    fontSize: 15,
    color: "white",
  },

  acount: {
    flexDirection: "row",
    marginTop: 20,
  },

  registerText: {
    color: "#B22222",
    textDecorationLine: "underline",
  },
});
