import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from "react-native";
import React from "react";
import { useDispatch } from "react-redux";
import { convertCurrency } from "../util/convert";
import { FontAwesome } from "@expo/vector-icons";

export default function MenuDetail({ route }) {
  const dispatch = useDispatch();

  const addToCart = async () => {
    Alert.alert("Good Job", "Menu successfully added to cart");
    dispatch({ type: "ADD_TO_CART", payload: route.params });
    console.log("Berhasil Ditambahkan ke cart");
  };

  return (
    <View style={styles.container}>
      <Image
        style={{
          width: 385,
          height: 300,
          borderRadius: 10,
        }}
        source={{
          uri: `${route.params.image}`,
        }}
      />
      <Text style={styles.title}>{route.params.name}</Text>

      <View style={styles.content}>
        <Text style={styles.discount}>
          Diskon{"\n"} {route.params.discount}%
        </Text>
        <Text style={styles.discount}>
          Price{"\n"} {convertCurrency(route.params.price)}
        </Text>
        <Text style={styles.rating}>
          Rating{"\n"}
          <FontAwesome name="star" size={17} color="orange" /> {""} 4.8
        </Text>
      </View>
      <Text style={styles.titledesc}>Description</Text>
      <Text style={styles.description}>{route.params.description}</Text>

      {/* {getStoreData("role") === "admin" ? (
        <View></View>
      ) : (
        <View> */}
      <TouchableOpacity style={styles.cartBtn} onPress={addToCart}>
        <Text style={styles.cartText}>
          <FontAwesome name="shopping-cart" size={25} color="white" /> {""} Add
          to Cart
        </Text>
      </TouchableOpacity>
      {/* </View>
      )} */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },

  title: {
    marginTop: 20,
    marginBottom: 5,
    fontSize: 23,
    fontWeight: "bold",
    textAlign: "center",
  },

  content: {
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "red",
    marginTop: 10,
    alignSelf: "center",
    borderRadius: 8,
  },

  discount: {
    fontSize: 14,
    borderRightWidth: 1,
    borderColor: "red",
    textAlign: "center",
    padding: 10,
    paddingHorizontal: 15,
  },

  rating: {
    fontSize: 14,
    padding: 10,
    textAlign: "center",
    paddingHorizontal: 15,
  },

  titledesc: {
    marginTop: 25,
    fontSize: 17,
    fontWeight: "bold",
  },

  description: {
    marginTop: 10,
    fontSize: 15,
    textAlign: "justify",
  },

  cartBtn: {
    width: "100%",
    height: 50,
    alignSelf: "center",
    marginTop: 25,
    borderRadius: 10,
    backgroundColor: "#b22222",
  },

  cartText: {
    fontWeight: "bold",
    fontSize: 18,
    textAlign: "center",
    marginTop: 10,
    color: "white",
  },
});
