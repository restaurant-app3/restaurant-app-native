import { View, StyleSheet, FlatList, TextInput } from "react-native";
import React, { useState } from "react";
import { FontAwesome5 } from "@expo/vector-icons";
import { instance as axios } from "../util/api";
import { getStoreData } from "../util/Util";
import Menu from "../components/Menu";

export default function SearchBar({ navigation }) {
  const [inputText, setInputText] = useState("");
  const [query, setQuery] = useState([]);

  const getSearching = async (txt) => {
    try {
      const { data, status } = await axios.get(
        `/api/menus/search?name=${txt}`,
        {
          headers: {
            Authorization: `Bearer ${await getStoreData("token")}`,
          },
        }
      );
      if (status === 200) {
        setQuery(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchWrapperStyle}>
        <TextInput
          placeholder="Search"
          style={styles.searchInputStyle}
          onChangeText={(text) => {
            setInputText(text);
            getSearching(text);
          }}
          value={inputText}
        />
        <FontAwesome5
          name="search"
          size={24}
          color="#b22222"
          style={styles.iconStyle}
        />
      </View>

      <FlatList
        data={query}
        renderItem={({ item }) => <Menu item={item} navigation={navigation} />}
        keyExtractor={(item) => "" + item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  searchWrapperStyle: {
    flexDirection: "row",
    padding: 15,
  },

  iconStyle: {
    marginTop: 12,
    marginHorizontal: 10,
  },

  searchInputStyle: {
    flex: 1,
    fontSize: 16,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 5,
    borderWidth: 3,
    borderColor: "#b22222",
  },
});
