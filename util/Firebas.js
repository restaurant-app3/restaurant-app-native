// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD5uHfpf4JZPwxow5e-S2wTjY7f5NwOqNw",
  authDomain: "resto-app-edc8f.firebaseapp.com",
  projectId: "resto-app-edc8f",
  storageBucket: "resto-app-edc8f.appspot.com",
  messagingSenderId: "166976667808",
  appId: "1:166976667808:web:c4b08876e7eeb0e11709c5",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
// Initialize Cloud Storage and get a reference to the service
export const storage = getStorage(app);
