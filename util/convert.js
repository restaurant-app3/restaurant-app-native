const convertCurrency = (x) => {
  return "Rp " + x.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

export { convertCurrency };
