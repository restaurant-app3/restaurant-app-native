const data = {
  carts: [],
  totalCart: 0,
};

const CartReducer = (state = data, action) => {
  switch (action.type) {
    case "ADD_TO_CART":
      if (state.totalCart == 0) {
        let cart = {
          id: action.payload.id,
          quantity: 1,
          name: action.payload.name,
          image: action.payload.image,
          price: action.payload.price,
          discount: action.payload.discount,
        };
        state.carts.push(cart);
      } else {
        let check = false;
        state.carts.map((item, key) => {
          if (item.id == action.payload.id) {
            state.carts[key].quantity++;
            check = true;
          }
        });
        if (!check) {
          let _cart = {
            id: action.payload.id,
            quantity: 1,
            name: action.payload.name,
            image: action.payload.image,
            price: action.payload.price,
            discount: action.payload.discount,
          };
          state.carts.push(_cart);
        }
      }
      return {
        ...state,
        totalCart: state.totalCart + 1,
      };

    case "RESET_CART":
      return { carts: [], totalCart: 0 };

    case "DELETE_CART":
      return {
        carts: state.carts.filter((x) => x.id !== action.payload),
        totalCart: 0,
      };
    default:
      return state;
  }
};

export default CartReducer;
